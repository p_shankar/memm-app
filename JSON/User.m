//
//  User.m
//  Memm
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "User.h"

@implementation User

- (id)initWithDictionary:(NSDictionary *)dict {
    [self setName:dict[@"Name"]];
    NSMutableDictionary *imageDict = dict[@"ProfilePic"];
    [self setImagePath:[HOST stringByAppendingString:imageDict[@"Path"]]];
    [self setUserId: dict[@"Id"]];
    [self setActivePlayground:dict[@"ActivePlaygroundName"]];
    [self setUsername:dict[@"Username"]];
    [self setDateOfBirth:dict[@"DateOfBirth"]];
    return self;
}

-(NSString *)fixDate:(NSString *)jsonDate {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    NSString *formattedString = [jsonDate substringToIndex:19];
    
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [df setLocale:posix];
    
    NSDate *date = [df dateFromString: formattedString];
    
    [df setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateStr = [df stringFromDate:date];
    return dateStr;
}

@end
