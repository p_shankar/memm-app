//
//  PostDetailViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 29/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import "CommentTableViewCell.h"
#import "CredentialStore.h"
#import "Constants.h"

@interface PostDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>

@property (strong, nonatomic) Post *post;

@property (weak, nonatomic) IBOutlet UITextField *commentTextField;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UITableView *tableView;

- (void) initWithPost;

@end
