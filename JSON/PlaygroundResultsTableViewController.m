//
//  PlaygroundResultsTableViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 26/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "PlaygroundResultsTableViewController.h"

@interface PlaygroundResultsTableViewController ()

@end

@implementation PlaygroundResultsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _headers = [NSMutableArray arrayWithObjects:@"My Playgrounds", @"Nearby Playgrounds", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _filteredPlaygrounds.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_filteredPlaygrounds objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier2];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier2];
    }
    [cell setBackgroundColor:[UIColor colorWithRed:171.0/255 green:189.0/255 blue:167.0/255 alpha:1]];
    
    Playground *playground = [[_filteredPlaygrounds objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self configureCell:cell forPlayground:playground];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
    return [_headers objectAtIndex:section];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
