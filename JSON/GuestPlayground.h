//
//  Playground.h
//  JSON
//
//  Created by Andri Thorhallsson on 21/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GuestPlayground : NSObject

@property (nonatomic, retain) NSString *playgroundId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic) NSInteger *range;

@end
