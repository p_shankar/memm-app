//
//  Playground.h
//  Memm
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Playground : NSObject

@property (nonatomic, retain) NSString *playgroundId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic) NSInteger range;
@property (nonatomic) NSNumber* lat;
@property (nonatomic) NSNumber* lon;
@property (nonatomic, assign) BOOL activeFriends;
@property (nonatomic, assign) BOOL currentUserIsCheckedIn;
@property (nonatomic, assign) BOOL empty;

- (id)initWithDictionary:(NSDictionary *)thePlayground;

@end
