//
//  PlaygroundInfoViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 14/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Constants.h"

@interface PlaygroundInfoViewController : UIViewController

@property (nonatomic, retain) NSString *playgroundId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic) NSInteger *range;
@property (nonatomic) NSNumber *lat;
@property (nonatomic) NSNumber *lon;
@property (nonatomic, retain) NSMutableArray *activeFriends;
@property (nonatomic, assign) BOOL currentUserIsCheckedIn;
@property (nonatomic, assign) BOOL currentUserIsFollowing;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;
@property (weak, nonatomic) IBOutlet UIView *infoBackgroundView;

-(void)fetchPlaygroundInfo;

@end
