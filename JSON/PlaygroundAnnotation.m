//
//  PlaygroundAnnotation.m
//  Memm
//
//  Created by Andri Thorhallsson on 06/08/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "PlaygroundAnnotation.h"

@implementation PlaygroundAnnotation
@synthesize coordinate, title, imageName;

- (id)initWithPlayground:(Playground *)playground {
    self = [super init];
    
    if (self) {
        self.title = playground.name;
        
        self.playgroundId = playground.playgroundId;
        
        CLLocationCoordinate2D playgroundLocation;
        playgroundLocation.latitude = playground.lat.doubleValue;
        playgroundLocation.longitude = playground.lon.doubleValue;
        
        coordinate = playgroundLocation;
        
        if ([playground currentUserIsCheckedIn]) {
            [self setImageName:@"LocationWhite"];
        }
        else if ([playground activeFriends]) {
            [self setImageName:@"LocationGreen"];
        }
        else if ([playground empty]) {
            [self setImageName:@"LocationRed"];
        }
        else {
            [self setImageName:@"LocationYellow"];
        }
    }
    return self;
}

- (MKAnnotationView *)annotationView {
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"PlaygroundAnnotationId"];
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.image = [UIImage imageNamed:self.imageName];
    return annotationView;
}


@end
