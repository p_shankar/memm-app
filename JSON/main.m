//
//  main.m
//  JSON
//
//  Created by Andri Thorhallsson on 21/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
