//
//  FeedTableViewController.h
//  JSON
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import "FeedCell.h"
#import "User.h"
#import "PostDetailViewController.h"

@interface FeedTableViewController : UITableViewController {
    NSMutableArray *feed;
}

- (void)fetchFeed;
- (void)configureCell:(FeedCell *)cell forPost:(Post *)post;

@end
