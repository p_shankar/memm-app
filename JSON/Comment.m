//
//  Comment.m
//  Memm
//
//  Created by Andri Thorhallsson on 29/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "Comment.h"
#import "DateTools.h"

@implementation Comment

- (id)initWithDictionary:(NSDictionary *)dict {
    User *author = [[User alloc] initWithDictionary:dict[@"Author"]];
    [self setAuthor:author];
    [self setText:dict[@"Text"]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    
    NSDate *date = [dateFormat dateFromString:dict[@"DateCreated"]];
    NSString *ago = [date timeAgoSinceNow];
    
    [self setDateCreated:ago];
    [self setCommentId:dict[@"Id"]];
    
    return self;
}

@end
