//
//  RegisterViewController.m
//  JSON
//
//  Created by Andri Thorhallsson on 30/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

- (IBAction)cancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)confirmPressed:(id)sender {
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy"];
    NSString *dateString = [df stringFromDate:self.datePicker.date];
    
    NSDictionary* jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [self.userNameTextField text],
                              @"UserName",
                              [self.passwordTextField text],
                              @"Password",
                              [self.confirmPasswordTextField text],
                              @"ConfirmPassword",
                              [self.fullNameTextField text],
                              @"Name",
                              [self.emailTextField text],
                              @"Email",
                              dateString,
                              @"DateOfBirth",
                              nil];
    
    //convert object to data
    NSError *error;
    NSData* postData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"/api/Account/Register"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:HOST forHTTPHeaderField:@"Referer"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&requestResponse
                                      error:nil];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) requestResponse;
    
    if ((long)[httpResponse statusCode] == 200)
    {
        [self performSegueWithIdentifier:@"registerSuccessful" sender:self];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ups" message:@"Something went wrong. Please try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
