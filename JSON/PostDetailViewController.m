//
//  PostDetailViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 29/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "PostDetailViewController.h"

@interface PostDetailViewController ()

@end

@implementation PostDetailViewController
@synthesize post;

- (void)viewDidLoad {
    [super viewDidLoad];
    [_tableView registerNib:[UINib nibWithNibName:@"CommentTableViewCell" bundle:nil] forCellReuseIdentifier:@"CommentCellId"];
    [self initWithPost];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [post.comments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCellId"];
    
    if (cell == nil) {
        cell = [[CommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommentCellId"];
    }
    UIView *whiteRoundedCornerView = [[UIView alloc] initWithFrame:CGRectMake(5 , 10, self.view.bounds.size.width-10, 100)];
    whiteRoundedCornerView.backgroundColor = ([UIColor memmBeige]);
    whiteRoundedCornerView.layer.shadowOpacity = 1.55;
    whiteRoundedCornerView.layer.shadowColor = (__bridge CGColorRef)([UIColor colorWithRed:66.0f/255.0f green:79.0f/255.0f blue:91.0f/255.0f alpha:1.0f]);
    whiteRoundedCornerView.layer.cornerRadius = 10.0;
    whiteRoundedCornerView.layer.shadowOffset = CGSizeMake(-1, -1);
    whiteRoundedCornerView.layer.shadowOpacity = 0.5;
    [cell.contentView addSubview:whiteRoundedCornerView];
    [cell.contentView sendSubviewToBack:whiteRoundedCornerView];
    
    cell.authorLabel.text = [[[post.comments objectAtIndex:indexPath.row] author] name];
    cell.textView.text = [[post.comments objectAtIndex:indexPath.row] text];
    cell.timeLabel.text = [[post.comments objectAtIndex:indexPath.row] dateCreated];
    NSString *imageUrl = [[[post.comments objectAtIndex:indexPath.row] author] imagePath];
    NSURL *myUrl = [NSURL URLWithString:imageUrl];
    NSData *imageData = [NSData dataWithContentsOfURL:myUrl];
    UIImage *image = [UIImage imageWithData:imageData];
    [cell.imageView setImage:image];
    //
    cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
    return cell;
}

- (void)updatePost {
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/GetPost?postId=%@", post.postId]];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSError *error;
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:&error];
    NSMutableDictionary *thePost = [NSJSONSerialization
                                     JSONObjectWithData:requestHandler
                                     options:NSJSONReadingMutableContainers
                                     error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        Post *newPost = [[Post alloc ] initWithDictionary:thePost];
        [self setPost:newPost];
        [self initWithPost];
    }
}

- (void)initWithPost {
    [_nameLabel setText:post.author.name];
    [_timeLabel setText:post.dateCreated];
    NSString *imageUrl = post.author.imagePath;
    NSURL *myUrl = [NSURL URLWithString:imageUrl];
    NSData *imageData = [NSData dataWithContentsOfURL:myUrl];
    UIImage *image = [UIImage imageWithData:imageData];
    _imageView.layer.cornerRadius = _imageView.frame.size.width/2;
    [_imageView setImage:image];
    [_textView setText:post.text];
    [_tableView reloadData];
    // Scrolla nedst
  //  NSIndexPath* ipath = [NSIndexPath indexPathForRow: post.comments.count-1 inSection: 0];
  //  [_tableView scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
}

- (IBAction)commentButtonPressed:(id)sender {
    CredentialStore *store = [[CredentialStore alloc] init];
    NSDictionary* jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [_commentTextField text],
                              @"commentText",
                              self.post.postId,
                              @"postId",
                              nil];
    //convert object to data
    NSError *jsonError;
    NSData* postData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted error:&jsonError];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/AddCommentToPostUser"]];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:HOST forHTTPHeaderField:@"Referer"];
    [request setHTTPBody:postData];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSError *error;
    NSURLResponse *requestResponse;
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&requestResponse
                                      error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) requestResponse;
    if ((long)[httpResponse statusCode] == 200) {
        [_commentTextField setText:@""];
        [self updatePost];
    }
    else {
        NSLog(@"%@", [error localizedDescription]);
    }
}
/*
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [self.view endEditing:YES];
    return YES;
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.view.contentInset = contentInsets;
    self.view.scrollIndicatorInsets = contentInsets;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
*/

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
