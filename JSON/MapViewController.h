//
//  MapViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 23/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Constants.h"
#import "PlaygroundAnnotation.h"

@interface MapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate> {
    NSMutableArray *nearbyPlaygrounds;
}

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *manager;

-(void)getNearbyPlaygrounds;

@end
