//
//  User.h
//  Memm
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface User : NSObject

@property (nonatomic, retain) NSString *userId;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *dateOfBirth;
@property (nonatomic, retain) NSString *imagePath;
@property (nonatomic, retain) NSString *activePlayground;
@property (nonatomic, retain) NSString *username;


- (id)initWithDictionary:(NSDictionary *)dict;

@end
