//
//  Post.h
//  Memm
//
//  Created by Andri Thorhallsson on 24/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Constants.h"
#import "Comment.h"

@interface Post : NSObject

@property (nonatomic, retain) NSString *postId;
@property (nonatomic, retain) User *author;
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *dateCreated;
@property (nonatomic, retain) NSString *dateModified;

@property (nonatomic, retain) NSMutableArray *comments;
@property (nonatomic, assign) BOOL onPlayground;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
