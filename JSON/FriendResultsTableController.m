//
//  FriendResultsTableController.m
//  Memm
//
//  Created by Andri Thorhallsson on 20/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "FriendResultsTableController.h"
#import "User.h"

@interface FriendResultsTableController ()

@end

@implementation FriendResultsTableController

- (void)viewDidLoad {
    [super viewDidLoad];
    _headers = [NSArray arrayWithObjects:@"Friends", @"Requests",@"Strangers", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.filteredUsers.count;
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
    return [_headers objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.filteredUsers objectAtIndex:section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
 
    User *friend = [[self.filteredUsers objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self configureCell:cell forUser:friend];
 
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
