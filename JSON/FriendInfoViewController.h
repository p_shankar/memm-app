//
//  FriendInfoViewController.h
//  Memm
//
//  Created by Andri Thorhallsson on 14/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "User.h"
#import "DateTools.h"

@interface FriendInfoViewController : UIViewController

@property (nonatomic, retain) User *user;

@property (nonatomic, assign) BOOL didSendFriendRequest;
@property (nonatomic, assign) BOOL hasPendingRequestFromUser;
@property (nonatomic, assign) BOOL areFriends;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirthLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton *declineButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *infoBackgroundView;

-(void)fetchUserInfo;

@end
