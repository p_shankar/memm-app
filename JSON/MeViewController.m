//
//  MeViewController.m
//  JSON
//
//  Created by Andri Thorhallsson on 07/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "MeViewController.h"

@interface MeViewController ()

@end

@implementation MeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"/rpcapi/rpc/GetUserInfo"]]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSMutableDictionary *jsonResponse = [NSJSONSerialization
                                         JSONObjectWithData:requestHandler
                                         options:NSJSONReadingMutableContainers
                                         error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        [_nameLabel setText:jsonResponse[@"Name"]];
        // Year old
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        NSDate *dob = [dateFormat dateFromString:jsonResponse[@"DateOfBirth"]];
        NSDate *now = [NSDate date];
        NSInteger years = [now yearsFrom:dob];
        NSString *age = [NSString stringWithFormat:@"%ld years old",(long)years];
        [_dateLabel setText:age];
        // Image
        NSMutableDictionary *pImage = jsonResponse[@"ProfilePic"];
        NSString *path = pImage[@"Path"];
        NSString *filePath = [HOST stringByAppendingString:path];
        NSURL *myUrl = [NSURL URLWithString:filePath];
        [self downloadImageWithURL:myUrl completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                _imageView.contentMode = UIViewContentModeScaleAspectFill;
                _imageView.clipsToBounds = YES;
                _imageView.image = image;
            }
        }];
    }
    [_imageView.layer setBorderColor: [[UIColor memmBeige] CGColor]];
    [_imageView.layer setBorderWidth: 8.0];
     _imageView.layer.cornerRadius = _imageView.frame.size.width/2;
    _infoBackground.layer.cornerRadius = 8;
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES, image);
                               } else{
                                   completionBlock(NO, nil);
                               }
                           }];
}

- (IBAction)changeProfilePicturePressed:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerCameraDeviceFront;
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _imageView.image = chosenImage;
    
    [self uploadImage:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)uploadImage:(UIImage *)image {
    NSString *boundary = @"blabla";
    CredentialStore *store = [[CredentialStore alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSMutableData *postData = [NSMutableData data];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"/rpcapi/rpc/uploadprofilepicture"]]];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    
    [postData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", @"upload"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:imageData];
    [postData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postData appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
   
    [request setHTTPBody:postData];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSMutableDictionary *jsonResponse = [NSJSONSerialization
                                         JSONObjectWithData:requestHandler
                                         options:NSJSONReadingMutableContainers
                                         error:&error];
    NSLog(@"%@",requestResponse);
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        NSLog(@"%@", jsonResponse);
    }
}

- (IBAction)logOutPressed {
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"api/Account/Logout"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&requestResponse
                                      error:nil];
  //  NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) requestResponse;
    // TODO: find out why response is nil
 //   if ((long)[httpResponse statusCode] == 200) {
        [store clearSavedCredentials];
        [self performSegueWithIdentifier:@"logOutConfirmed" sender:self];
  //  }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before na
 vigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
