//
//  FriendInfoViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 14/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "FriendInfoViewController.h"
#import "CredentialStore.h"

@interface FriendInfoViewController ()

@end

@implementation FriendInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchUserInfo];
    if (_areFriends) {
        [self.actionButton setTitle:@"Remove Friend" forState:UIControlStateNormal];
    }
    else if (_didSendFriendRequest) {
        [self.actionButton setTitle:@"Cancel Request" forState:UIControlStateNormal];
    }
    else if (_hasPendingRequestFromUser) {
        self.acceptButton.hidden = false;
        self.declineButton.hidden = false;
        self.actionButton.hidden = true;
    }
    else {
        [self.actionButton setTitle:@"Add Friend" forState:UIControlStateNormal];
    }
    
    [_imageView.layer setBorderColor: [[UIColor memmBeige] CGColor]];
    [_imageView.layer setBorderWidth: 8.0];
    _imageView.layer.cornerRadius = _imageView.frame.size.width/2;
     _infoBackgroundView.layer.cornerRadius = 8;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionButtonPressed:(id)sender {
    CredentialStore *store = [[CredentialStore alloc] init];
    
    if (_areFriends) {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/RemoveFriend?friendId=%@", _user.userId]];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
        [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
        
        NSURLResponse *requestResponse;
        NSError *error;
        [NSURLConnection sendSynchronousRequest:request
                              returningResponse:&requestResponse
                                          error:&error];
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        else {
            [self.actionButton setTitle:@"Add Friend" forState:UIControlStateNormal];
            _areFriends = false;
        }
    }
    else if (_didSendFriendRequest) {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@/rpcapi/rpc/RemoveFriend?friendId=%@", HOST, _user.userId];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
        [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
        
        NSURLResponse *requestResponse;
        NSError *error;
        [NSURLConnection sendSynchronousRequest:request
                              returningResponse:&requestResponse
                                          error:&error];
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        else {
            [self.actionButton setTitle:@"Add Friend" forState:UIControlStateNormal];
            _didSendFriendRequest = false;
        }
    }
    else {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSString *urlString = [NSString stringWithFormat:@"%@/rpcapi/rpc/AddFriend?friendId=%@", HOST, _user.userId];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
        [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
        
        NSURLResponse *requestResponse;
        NSError *error;
        [NSURLConnection sendSynchronousRequest:request
                              returningResponse:&requestResponse
                                          error:&error];
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        else {
            [self.actionButton setTitle:@"Cancel Request" forState:UIControlStateNormal];
            _didSendFriendRequest = true;
        }
    }
}

- (void)fetchUserInfo {
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [NSString stringWithFormat:@"%@/rpcapi/rpc/UserPage?username=%@", HOST, _user.username];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"Post"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSMutableDictionary *model = [NSJSONSerialization
                                       JSONObjectWithData:requestHandler
                                       options:NSJSONReadingMutableContainers
                                       error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        User *newUser = [[User alloc] initWithDictionary:model[@"User"]];
        
        [_nameLabel setText:newUser.name];
        // Age
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        NSDate *dateOfBirth = [dateFormat dateFromString:newUser.dateOfBirth];
        NSInteger age = [[NSDate date] yearsFrom:dateOfBirth];
        [_dateOfBirthLabel setText:[NSString stringWithFormat:@"%ld years old",(long)age]];
        
        NSURL *myUrl = [NSURL URLWithString:newUser.imagePath];
        [self downloadImageWithURL:myUrl completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                _imageView.contentMode = UIViewContentModeScaleAspectFill;
                _imageView.clipsToBounds = YES;
                _imageView.image = image;
            }
        }];
        _areFriends = [model[@"areFriends"] boolValue];
        _didSendFriendRequest = [model[@"didSendFriendRequest"] boolValue];
        _hasPendingRequestFromUser = [model[@"hasPendingRequestFromUser"] boolValue];
    }
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES, image);
                               } else{
                                   completionBlock(NO, nil);
                               }
                           }];
}

- (IBAction)acceptPressed:(id)sender {
    CredentialStore *store = [[CredentialStore alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/AcceptFriend?friendId=%@", _user.userId]];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSError *error;
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&requestResponse
                                      error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        [self.actionButton setTitle:@"Remove Friend" forState:UIControlStateNormal];
        self.actionButton.hidden = false;
        self.acceptButton.hidden = true;
        self.declineButton.hidden = true;
        _didSendFriendRequest = false;
        _areFriends = true;
        
    }
}

- (IBAction)declinePressed:(id)sender {
    CredentialStore *store = [[CredentialStore alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/RemoveFriend?friendId=%@", _user.userId]];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSError *error;
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&requestResponse
                                      error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        [self.actionButton setTitle:@"Add Friend" forState:UIControlStateNormal];
        self.actionButton.hidden = false;
        self.acceptButton.hidden = true;
        self.declineButton.hidden = true;
        _didSendFriendRequest = false;
        _areFriends = false;
        _hasPendingRequestFromUser = false;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
