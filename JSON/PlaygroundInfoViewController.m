//
//  PlaygroundInfoViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 14/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "PlaygroundInfoViewController.h"
#import "CredentialStore.h"
#import "PlaygroundListTableViewController.h"

@interface PlaygroundInfoViewController ()

@end

@implementation PlaygroundInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fetchPlaygroundInfo];
    if (_currentUserIsCheckedIn) {
        [self.checkButton setTitle:@"Check out" forState:UIControlStateNormal];
    }
    
    if (_currentUserIsFollowing) {
        [self.followButton setTitle:@"Unfollow" forState:UIControlStateNormal];
    }
    
    [_imageView.layer setBorderColor: [[UIColor memmBeige] CGColor]];
    [_imageView.layer setBorderWidth: 8.0];
    _imageView.layer.cornerRadius = _imageView.frame.size.width/2;
    _infoBackgroundView.layer.cornerRadius = 8;
}

- (void)viewWillAppear {
    [self fetchPlaygroundInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)followPressed:(id)sender {
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/FollowPlayground?playgroundId=%@", _playgroundId]];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSError *error;
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&requestResponse
                                      error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        if (_currentUserIsFollowing) {
            [self setCurrentUserIsFollowing:false];
            [self.followButton setTitle:@"Follow" forState:UIControlStateNormal];
        }
        else {
            [self setCurrentUserIsFollowing:true];
            [self.followButton setTitle:@"Unfollow" forState:UIControlStateNormal];
        }
    }
}

- (IBAction)checkPressed:(id)sender {
        CredentialStore *store = [[CredentialStore alloc] init];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/CheckInPlayground?playgroundId=%@", _playgroundId]];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
        [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
        NSError *error;
        NSURLResponse *requestResponse;
        [NSURLConnection sendSynchronousRequest:request
                              returningResponse:&requestResponse
                                          error:&error];
        if (error)
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        else {
            if (_currentUserIsCheckedIn) {
                [self setCurrentUserIsCheckedIn:false];
                [self.checkButton setTitle:@"Check In" forState:UIControlStateNormal];
            }
            else {
                [self setCurrentUserIsCheckedIn:true];
                [self.checkButton setTitle:@"Check Out" forState:UIControlStateNormal];
            }
        }
}

-(void)fetchPlaygroundInfo {
    CredentialStore *store = [[CredentialStore alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/PlaygroundInfo?id=%@", _playgroundId]];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSMutableDictionary *playground = [NSJSONSerialization
                                       JSONObjectWithData:requestHandler
                                       options:NSJSONReadingMutableContainers
                                       error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        [self setCurrentUserIsCheckedIn:[playground[@"CurrentUserIsCheckedIn"] boolValue]];
        [self setCurrentUserIsFollowing:[playground[@"CurrentUserIsFollowing"] boolValue]];
        [self setLat:[NSNumber numberWithDouble:[playground[@"Latitude"] doubleValue]]];
        [self setLon:[NSNumber numberWithDouble:[playground[@"Longitude"] doubleValue]]];
        
        // Image
        NSMutableDictionary *pImage = playground[@"PlaygroundImage"];
        NSURL *myUrl = [NSURL URLWithString:pImage[@"Path"]];
        [self downloadImageWithURL:myUrl completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                // change the image in the cell
                _imageView.contentMode = UIViewContentModeScaleAspectFill;
                _imageView.clipsToBounds = YES;
                _imageView.image = image;
            }
        }];
        
        // Name
        [_nameLabel setText:playground[@"Name"]];
        
        // Rating
        [_ratingLabel setText:[NSString stringWithFormat:@"%@",playground[@"Rating"]]];
    }
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES, image);
                               } else{
                                   completionBlock(NO, nil);
                               }
                           }];
}

- (IBAction)mapButtonPressed:(id)sender {
    if (_mapView == nil) {
        // Map
        _mapView = [[MKMapView alloc] initWithFrame:self.imageView.bounds];
        _mapView.showsUserLocation = NO;
        _mapView.mapType = MKMapTypeStandard;
        [self.view addSubview:_mapView];
        _mapView.layer.cornerRadius = _mapView.frame.size.width/2;
        _mapView.center = _imageView.center;
        [_mapView.layer setBorderColor: [[UIColor memmBeige] CGColor]];
        [_mapView.layer setBorderWidth: 8.0];
        _mapView.hidden = true;
        
        // Set zoom
        CLLocationCoordinate2D zoomLocation;
        zoomLocation.latitude = _lat.doubleValue;
        zoomLocation.longitude= _lon.doubleValue;
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 800, 800);
        [_mapView setRegion:viewRegion animated:NO];
        
        // Add an annotation
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = zoomLocation;
        point.title = _nameLabel.text;
        point.subtitle = @"Playground";
        [_mapView addAnnotation:point];
    }
    if (_mapView.hidden) {
        _mapView.hidden = false;
        [_mapButton setTitle:@"Hide Map" forState:UIControlStateNormal];
    }
    else {
        _mapView.hidden = true;
        [_mapButton setTitle:@"Show Map" forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
