//
//  CredentialStore.h
//  JSON
//
//  Created by Andri Thorhallsson on 30/06/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CredentialStore : NSObject

- (BOOL)isLoggedIn;
- (void)clearSavedCredentials;
- (NSString *)authToken;
- (void)setAuthToken:(NSString *)authToken;

@end