//
//  FriendResultsTableController.h
//  Memm
//
//  Created by Andri Thorhallsson on 20/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsBaseTableViewController.h"

@interface FriendResultsTableController : FriendsBaseTableViewController

@property (nonatomic, strong) NSArray *filteredUsers;
@property (nonatomic, strong) NSArray *headers;

@end
