//
//  Post.m
//  Memm
//
//  Created by Andri Thorhallsson on 24/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "Post.h"
#import "DateTools.h"

@implementation Post

- (id)initWithDictionary:(NSDictionary *)dict {
    _comments = [[NSMutableArray alloc] init];
    NSDictionary *allComments = dict[@"Comments"];
    
    for (NSDictionary *theComment in allComments) {
        Comment *newComment = [[Comment alloc] initWithDictionary:theComment];
        [_comments addObject:newComment];
    }
    
    User *author = [[User alloc ] initWithDictionary:dict[@"Author"]];
    [self setAuthor:author];
    
    [self setPostId:dict[@"Id"]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    
    NSDate *date = [dateFormat dateFromString:dict[@"DateModified"]];
    NSDate *dateCreated = [dateFormat dateFromString:dict[@"DateCreated"]];
    NSString *ago = [date timeAgoSinceNow];
    NSString *agoCreated = [dateCreated timeAgoSinceNow];
    
    [self setDateCreated:agoCreated];
    [self setDateModified:ago];
    
    [self setText:dict[@"Text"]];
    
    return self;
}

@end
