//
//  PostViewController.m
//  Memm
//
//  Created by Andri Thorhallsson on 24/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "PostViewController.h"
#import "CredentialStore.h"

@interface PostViewController ()

@end

@implementation PostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendPressed:(id)sender {
    CredentialStore *store = [[CredentialStore alloc] init];
   
    NSDictionary* jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              [_postTextField text],
                              @"postText",
                              nil];
    
    //convert object to data
    NSError *error;
    NSData* postData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[HOST stringByAppendingString:@"/rpcapi/rpc/AddPostToUser"]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:HOST forHTTPHeaderField:@"Referer"];
    [request setHTTPBody:postData];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    [NSURLConnection sendSynchronousRequest:request
                          returningResponse:&requestResponse
                                      error:nil];
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) requestResponse;
    
    if ((long)[httpResponse statusCode] == 200)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        NSLog(@"%@", requestResponse);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
