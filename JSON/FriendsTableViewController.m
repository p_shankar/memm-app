//
//  FriendsTableViewController.m
//  JSON
//
//  Created by Andri Thorhallsson on 09/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import "FriendsTableViewController.h"
#import "FriendInfoViewController.h"
#import "CredentialStore.h"
#import "User.h"
#import "FriendsBaseTableViewController.h"
#import "FriendResultsTableController.h"


@interface FriendsTableViewController () <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
@property (nonatomic, strong) UISearchController *searchController;

// our secondary search results table view
@property (nonatomic, strong) FriendResultsTableController *resultsTableController;

// for state restoration
@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;
@end

@implementation FriendsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _resultsTableController = [[FriendResultsTableController alloc] init];
    _searchController = [[UISearchController alloc] initWithSearchResultsController:self.resultsTableController];
    self.searchController.searchResultsUpdater = self;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    // we want to be the delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
    self.resultsTableController.tableView.delegate = self;
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    self.searchController.searchBar.delegate = self; // so we can monitor text changes + others
    
    // Search is now just presenting a view controller. As such, normal view controller
    // presentation semantics apply. Namely that presentation will walk up the view controller
    // hierarchy until it finds the root view controller or one that defines a presentation context.
    //
    self.definesPresentationContext = YES;  // know where you want UISearchController to be displayed
    
    // Set the background color of the results table to Green
    [self.resultsTableController.tableView setBackgroundColor:[UIColor colorWithRed:171.0/255 green:189.0/255 blue:167.0/255 alpha:1]];
    [self.resultsTableController.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fetchFriends];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sections count];
}

- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section {
    return [headers objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return [[sections objectAtIndex:section] count];
}

- (void)fetchFriends {
    CredentialStore *store = [[CredentialStore alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://test.memmapp.com/rpcapi/rpc/Friend"]];
    [request setHTTPMethod:@"Post"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSMutableDictionary *allFriends = [NSJSONSerialization
                                         JSONObjectWithData:requestHandler
                                         options:NSJSONReadingMutableContainers
                                         error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        friends = [[NSMutableArray alloc] init];
        requests = [[NSMutableArray alloc] init];
        
        NSMutableDictionary *requestsDict = allFriends[@"Requests"];
        NSMutableDictionary *friendsDict = allFriends[@"Friends"];
        
        for ( NSDictionary *theFriend in friendsDict )
        {
            [friends addObject:[[User alloc] initWithDictionary:theFriend]];
        }
        
        for ( NSDictionary *theFriend in requestsDict )
        {
            [requests addObject:[[User alloc] initWithDictionary:theFriend]];
        }
    }
    headers = [NSArray arrayWithObjects:@"Requests",@"Friends", nil];
    sections = [NSArray arrayWithObjects:requests, friends, nil];
    
    [[self tableView] reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    User *friend = [[self->sections objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [self configureCell:cell forUser:friend];
    
     cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width/2;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        User *selectedUser = [[sections objectAtIndex:indexPath.section]  objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"showFriendInfo" sender:selectedUser];
    }
    else {
        FriendResultsTableController *tableController = (FriendResultsTableController *)self.searchController.searchResultsController;
        User *selectedUser = [[tableController.filteredUsers objectAtIndex:indexPath.section]  objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"showFriendInfo" sender:selectedUser];
    }
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSMutableArray *searchFriends = [[NSMutableArray alloc] init];
    NSMutableArray *searchRequests = [[NSMutableArray alloc] init];
    NSMutableArray *searchStrangers = [[NSMutableArray alloc] init];
    
    // update the filtered array based on the search text
    NSString *searchText = searchController.searchBar.text;
    
    if (searchText.length < 1) {
        return;
    }
    
    // get the relevant users
    CredentialStore *store = [[CredentialStore alloc] init];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *urlString = [HOST stringByAppendingString:[NSString stringWithFormat:@"/rpcapi/rpc/SearchUsers?search=%@", searchText]];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"Post"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [store authToken]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request
                                                   returningResponse:&requestResponse
                                                               error:nil];
    NSError *error;
    NSArray *people = [NSJSONSerialization
                                   JSONObjectWithData:requestHandler
                                   options:NSJSONReadingMutableContainers
                                   error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        NSMutableDictionary *requestsDict;
        NSMutableDictionary *friendsDict;
        NSMutableDictionary *strangersDict;
    
        requestsDict = people[0];
        friendsDict = people[1];
        strangersDict = people[2];
        
        for ( NSDictionary *theFriend in friendsDict )
        {
            [searchFriends addObject:[[User alloc] initWithDictionary:theFriend]];
        }
        
        for ( NSDictionary *theFriend in requestsDict )
        {
            [searchRequests addObject:[[User alloc] initWithDictionary:theFriend]];
        }
        
        for ( NSDictionary *theFriend in strangersDict )
        {
            [searchStrangers addObject:[[User alloc] initWithDictionary:theFriend]];
        }
    }
    
    NSMutableArray *searchResults = [NSMutableArray arrayWithObjects: searchFriends, searchRequests, searchStrangers, nil];

    // hand over the filtered results to our search results table
    FriendResultsTableController *tableController = (FriendResultsTableController *)self.searchController.searchResultsController;
    tableController.filteredUsers = searchResults;
    [tableController.tableView reloadData];
}

/*
#pragma mark - UIStateRestoration

// we restore several items for state restoration:
//  1) Search controller's active state,
//  2) search text,
//  3) first responder

NSString *const ViewControllerTitleKey = @"ViewControllerTitleKey";
NSString *const SearchControllerIsActiveKey = @"SearchControllerIsActiveKey";
NSString *const SearchBarTextKey = @"SearchBarTextKey";
NSString *const SearchBarIsFirstResponderKey = @"SearchBarIsFirstResponderKey";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    // encode the view state so it can be restored later
    
    // encode the title
    [coder encodeObject:self.title forKey:ViewControllerTitleKey];
    
    UISearchController *searchController = self.searchController;
    
    // encode the search controller's active state
    BOOL searchDisplayControllerIsActive = searchController.isActive;
    [coder encodeBool:searchDisplayControllerIsActive forKey:SearchControllerIsActiveKey];
    
    // encode the first responser status
    if (searchDisplayControllerIsActive) {
        [coder encodeBool:[searchController.searchBar isFirstResponder] forKey:SearchBarIsFirstResponderKey];
    }
    
    // encode the search bar text
    [coder encodeObject:searchController.searchBar.text forKey:SearchBarTextKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];
    
    // restore the title
    self.title = [coder decodeObjectForKey:ViewControllerTitleKey];
    
    // restore the active state:
    // we can't make the searchController active here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerWasActive = [coder decodeBoolForKey:SearchControllerIsActiveKey];
    
    // restore the first responder status:
    // we can't make the searchController first responder here since it's not part of the view
    // hierarchy yet, instead we do it in viewWillAppear
    //
    _searchControllerSearchFieldWasFirstResponder = [coder decodeBoolForKey:SearchBarIsFirstResponderKey];
    
    // restore the text in the search field
    self.searchController.searchBar.text = [coder decodeObjectForKey:SearchBarTextKey];
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [segue.destinationViewController setUser:sender];
}

@end
