//
//  Comment.h
//  Memm
//
//  Created by Andri Thorhallsson on 29/07/15.
//  Copyright (c) 2015 Andri Thorhallsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Constants.h"

@interface Comment : NSObject

@property (nonatomic, retain) NSString *commentId;
@property (nonatomic, retain) User *author;
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *dateCreated;

- (id)initWithDictionary:(NSDictionary *)dict;

@end
